To improve my othello AI, I would first implement a scoring function that gives
a value to a given board position. This would just be number of stones of our
color minus the number of stones of the other color. Then, I would implement
minimax with alpha-beta pruning to keep the number of subtrees that must be
searched to a lower level. I would also implement a transposition table (a
hash table that basically keeps track of what boards we have seen so far and
their associated score). I would do preprocessing to fill out most of this
transposition table beforehand - just play many games with the random player
and store the associated scores. That way, the alpha-beta pruning would go much
faster.