#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    our_side = side;
    opponent_side = BLACK;
    if (our_side == BLACK)
    {
        opponent_side = WHITE;
    }

    board = Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

    // Do our opponent's move
    if (opponentsMove != NULL)
    {
        board.doMove(opponentsMove, opponent_side);
    }

    Move *next_move = new Move(-1, -1);

    // Find the first available move on the board
    for (int r = 0; r < 8; r++)
    {
        for (int c = 0; c < 8; c++)
        {
            Move move(r, c);

            if (board.checkMove(&move, our_side))
            {
                next_move->setX(r);
                next_move->setY(c);
                break;
            }

        }
    }

    if (next_move->getX() != -1)
    {
        board.doMove(next_move, our_side);
        return next_move;
    }

    return NULL;
}
